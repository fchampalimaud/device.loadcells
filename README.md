# New Repository Location #
This repository was moved. For the current and future developments please check the new location [here](https://github.com/harp-tech/device.loadcells).

## Licensing ##

Each subdirectory will contain a license or, possibly, a set of licenses if it involves both hardware and software.